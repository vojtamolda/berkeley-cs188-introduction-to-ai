
# UC Berkeley Course CS188 - Introduction to Artificial Intelligence (Fall 2018)


Lecture notes, syllabus, staff, schedule, projects and other materials can be downloaded from the [course webpage](https://inst.eecs.berkeley.edu/~cs188/fa18/index.html).
 - Lectures: [YouTube](https://inst.eecs.berkeley.edu/~cs188/fa18/index.html)
 - Schedule: [Berkeley CS188](https://inst.eecs.berkeley.edu/~cs188/fa18/schedule.html)
 - Projects: [Berkeley CS188](https://inst.eecs.berkeley.edu/~cs188/fa18/projects.html)
 - Homework: [Gradescope](https://www.gradescope.com/courses/33660)


## Homework and Projects

The Pac-Man projects were developed for CS 188. They apply an array of AI techniques to playing Pac-Man. However, these projects don't focus on building AI for video games. Instead, they teach foundational AI concepts, such as informed state-space search, probabilistic inference, and reinforcement learning. These concepts underly real-world application areas such as natural language processing, computer vision, and robotics.

![Pacman Demo](https://inst.eecs.berkeley.edu/~cs188/fa18/assets/images/pacman_game.gif)

We designed these projects with three goals in mind. The projects allow you to visualize the results of the techniques you implement. They also contain code examples and clear directions, but do not force you to wade through undue amounts of scaffolding. Finally, Pac-Man provides a challenging problem environment that demands creative solutions; real-world AI problems are challenging, and Pac-Man is too.

 - [X] **Project 0**: [UNIX/Python Tutorial](https://inst.eecs.berkeley.edu/~cs188/fa18/project0.html)  [`project/tutorial/`](project/tutorial/) - This short UNIX/Python tutorial introduces students to the Python programming language and the UNIX environment.

 - [X] **Project 1**: [Search](https://inst.eecs.berkeley.edu/~cs188/fa18/project1.html) [`project/search/`](project/search/) - Students implement depth-first, breadth-first, uniform cost, and A* search algorithms. These algorithms are used to solve navigation and traveling salesman problems in the Pacman world.

 - [X] **Mini-Contest 1**: [Multi-Agent Pacman](https://inst.eecs.berkeley.edu/~cs188/fa18/minicontest1.html) [`project/minicontest1/`](project/minicontest1/) - Students will apply the search algorithms and problems implemented in Project 1 to handle more difficult scenarios that include controlling multiple pacman agents and planning under time constraints

 - [X] **Project 2**: [Multi-Agent Search](https://inst.eecs.berkeley.edu/~cs188/fa18/project2.html) [`project/multiagent/`](project/multiagent/) - Classic Pacman is modeled as both an adversarial and a stochastic search problem. Students implement multiagent minimax and expectimax algorithms, as well as designing evaluation functions.

 - [X] **Mini-Contest 2**: [Multi-Agent Adversarial Pacman](https://inst.eecs.berkeley.edu/~cs188/fa18/minicontest2.html) [`project/minicontest2/`](project/minicontest2/) - This minicontest involves a multiplayer capture-the-flag variant of Pacman, where agents control both Pacman and ghosts in coordinated team-based strategies. Each team will try to eat the food on the far side of the map, while defending the food on their home side.

 - [X] **Project 3**: [Reinforcement Learning](https://inst.eecs.berkeley.edu/~cs188/fa18/project3.html) [`project/reinforcement/`](project/reinforcement/) - Students implement model-based and model-free reinforcement learning algorithms, applied to the AIMA textbook's Gridworld, Pacman, and a simulated crawling robot.

 - [X] **Project 4**: [Ghostbusters](https://inst.eecs.berkeley.edu/~cs188/fa18/project4.html) [`project/ghostbusters/`](project/ghostbusters/) - Probabilistic inference in a hidden Markov model tracks the movement of hidden ghosts in the Pacman world. Students implement exact inference using the forward algorithm and approximate inference via particle filters.

 - [X] **Project 5**: [Machine Learning](https://inst.eecs.berkeley.edu/~cs188/fa18/project5.html) [`project/machinelearning/`](project/machinelearning/) - Students implement the perceptron algorithm and neural network models, and apply the models to several tasks including digit classification.

 - [X] **Contest**: [Pac-Pack](http://pacpack.org/) [`project/contest/`](project/contest/) - Students create strategies for a cooperative team mate agent that has to avoid the ghosts and cooperatively eat all food pellets on the map.


## Prerequisites

Course programming assignments will be in Python. We do not assume that students have previous experience with the language, but we do expect you to learn the basics very rapidly.

 - **CS 61A** or **61B**: Prior computer programming experience is expected.
 - **CS 70** or **Math 55**: Facility with basic concepts of propositional logic and probability are expected. 


## Course Description

This course will introduce the basic ideas and techniques underlying the design of intelligent computer systems. A specific emphasis will be on the statistical and decision-theoretic modeling paradigm.

By the end of this course, you will have built autonomous agents that efficiently make decisions in fully informed, partially observable and adversarial settings. Your agents will draw inferences in uncertain environments and optimize actions for arbitrary reward structures. Your machine learning algorithms will classify handwritten digits and photographs. The techniques you learn in this course apply to a wide variety of artificial intelligence problems and will serve as the foundation for further study in any application area you choose to pursue.
