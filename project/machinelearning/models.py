import nn
import backend


class PerceptronModel(object):
    def __init__(self, dimensions: int):
        """
        Initialize a new Perceptron instance.

        A perceptron classifies data points as either belonging to a particular
        class (+1) or not (-1). `dimensions` is the dimensionality of the data.
        For example, dimensions=2 would mean that the perceptron must classify
        2D points.
        """
        self.w = nn.Parameter(1, dimensions)

    def get_weights(self) -> nn.Parameter:
        """
        Return a Parameter instance with the current weights of the perceptron.
        """
        return self.w

    def run(self, x: nn.Node) -> nn.DotProduct:
        """
        Calculates the score assigned by the perceptron to a data point x.

        Inputs:
            x: a node with shape (1 x dimensions)
        Returns: a node containing a single number (the score)
        """
        score = nn.DotProduct(self.w, x)
        return score

    def get_prediction(self, x: nn.Node) -> int:
        """
        Calculates the predicted class for a single data point `x`.

        Returns: 1 or -1
        """
        score = self.run(x)
        label = +1 if nn.as_scalar(score) >= 0 else -1
        return label

    def train(self, dataset: backend.Dataset):
        """
        Train the perceptron until convergence.
        """
        misclassified = True
        while misclassified is True:
            misclassified = False
            for x, y in dataset.iterate_once(1):
                y = nn.as_scalar(y)
                if y != self.get_prediction(x):
                    self.get_weights().update(x, y)
                    misclassified = True


class RegressionModel(object):
    """
    A neural network model for approximating a function that maps from real
    numbers to real numbers. The network should be sufficiently large to be able
    to approximate sin(x) on the interval [-2pi, 2pi] to reasonable precision.
    """
    def __init__(self):
        # Initialize your model parameters here
        self.epochs = 5_000
        self.lr = 0.005
        self.bs = 10

        self.w0 = nn.Parameter(1, 5)
        self.b0 = nn.Parameter(1, 5)
        self.w1 = nn.Parameter(5, 5)
        self.b1 = nn.Parameter(1, 5)
        self.w2 = nn.Parameter(5, 1)
        self.b2 = nn.Parameter(1, 1)

    def run(self, x: nn.Constant) -> nn.Node:
        """
        Runs the model for a batch of examples.

        Inputs:
            x: a node with shape (batch_size x 1)
        Returns:
            A node with shape (batch_size x 1) containing predicted y-values
        """
        xw0 = nn.Linear(x, self.w0)
        xwb0 = nn.AddBias(xw0, self.b0)
        a0 = nn.ReLU(xwb0)

        xw1 = nn.Linear(a0, self.w1)
        xwb1 = nn.AddBias(xw1, self.b1)
        a1 = nn.ReLU(xwb1)

        xw2 = nn.Linear(a1, self.w2)
        xwb2 = nn.AddBias(xw2, self.b2)
        return xwb2

    def get_loss(self, x: nn.Constant, y: nn.Constant) -> nn.SquareLoss:
        """
        Computes the loss for a batch of examples.

        Inputs:
            x: a node with shape (batch_size x 1)
            y: a node with shape (batch_size x 1), containing the true y-values
                to be used for training
        Returns: a loss node
        """
        loss = nn.SquareLoss(self.run(x), y)
        return loss

    def train(self, dataset: backend.Dataset):
        """
        Trains the model.
        """
        for epoch in range(self.epochs):
            for x, y in dataset.iterate_once(self.bs):
                loss = self.get_loss(x, y)
                grads = nn.gradients(loss, [self.w0, self.b0, self.w1, self.b1, self.w2, self.b2])
                self.w0.update(grads[0], -self.lr)
                self.b0.update(grads[1], -self.lr)
                self.w1.update(grads[2], -self.lr)
                self.b1.update(grads[3], -self.lr)
                self.w2.update(grads[4], -self.lr)
                self.b2.update(grads[5], -self.lr)


class DigitClassificationModel(object):
    """
    A model for handwritten digit classification using the MNIST dataset.

    Each handwritten digit is a 28x28 pixel grayscale image, which is flattened
    into a 784-dimensional vector for the purposes of this model. Each entry in
    the vector is a floating point number between 0 and 1.

    The goal is to sort each digit into one of 10 classes (number 0 through 9).

    (See RegressionModel for more information about the APIs of different
    methods here. We recommend that you implement the RegressionModel before
    working on this part of the project.)
    """
    def __init__(self):
        # Initialize your model parameters here
        self.epochs = 5
        self.lr = 0.005
        self.bs = 5

        self.w0 = nn.Parameter(28 * 28, 512)
        self.b0 = nn.Parameter(1, 512)
        self.w1 = nn.Parameter(512, 256)
        self.b1 = nn.Parameter(1, 256)
        self.w2 = nn.Parameter(256, 10)
        self.b2 = nn.Parameter(1, 10)

    def run(self, x: nn.Constant) -> nn.Node:
        """
        Runs the model for a batch of examples.

        Your model should predict a node with shape (batch_size x 10),
        containing scores. Higher scores correspond to greater probability of
        the image belonging to a particular class.

        Inputs:
            x: a node with shape (batch_size x 784)
        Output:
            A node with shape (batch_size x 10) containing predicted scores
                (also called logits)
        """
        xw0 = nn.Linear(x, self.w0)
        xwb0 = nn.AddBias(xw0, self.b0)
        a0 = nn.ReLU(xwb0)

        xw1 = nn.Linear(a0, self.w1)
        xwb1 = nn.AddBias(xw1, self.b1)
        a1 = nn.ReLU(xwb1)

        xw2 = nn.Linear(a1, self.w2)
        xwb2 = nn.AddBias(xw2, self.b2)
        return xwb2

    def get_loss(self, x: nn.Constant, y: nn.Constant) -> nn.SoftmaxLoss:
        """
        Computes the loss for a batch of examples.

        The correct labels `y` are represented as a node with shape
        (batch_size x 10). Each row is a one-hot vector encoding the correct
        digit class (0-9).

        Inputs:
            x: a node with shape (batch_size x 784)
            y: a node with shape (batch_size x 10)
        Returns: a loss node
        """
        logits = self.run(x)
        loss = nn.SoftmaxLoss(logits, y)
        return loss

    def train(self, dataset: backend.Dataset):
        """
        Trains the model.
        """
        for epoch in range(self.epochs):
            for x, y in dataset.iterate_once(self.bs):
                loss = self.get_loss(x, y)
                grads = nn.gradients(loss, [self.w0, self.b0, self.w1, self.b1, self.w2, self.b2])
                self.w0.update(grads[0], -self.lr)
                self.b0.update(grads[1], -self.lr)
                self.w1.update(grads[2], -self.lr)
                self.b1.update(grads[3], -self.lr)
                self.w2.update(grads[4], -self.lr)
                self.b2.update(grads[5], -self.lr)
            if dataset.get_validation_accuracy() > 0.97:
                break


class LanguageIDModel(object):
    """
    A model for language identification at a single-word granularity.

    (See RegressionModel for more information about the APIs of different
    methods here. We recommend that you implement the RegressionModel before
    working on this part of the project.)
    """
    def __init__(self):
        # Our dataset contains words from five different languages, and the
        # combined alphabets of the five languages contain a total of 47 unique
        # characters.
        # You can refer to self.num_chars or len(self.languages) in your code
        self.languages = ["English", "Spanish", "Finnish", "Dutch", "Polish"]
        self.num_hidden = 384
        self.num_chars = 47
        self.epochs = 20
        self.lr = 0.001
        self.bs = 5

        # Initialize your model parameters here
        self.w0 = nn.Parameter(self.num_chars, self.num_hidden)
        self.wx = nn.Parameter(self.num_chars, self.num_hidden)
        self.wh = nn.Parameter(self.num_hidden, self.num_hidden)
        self.wf = nn.Parameter(self.num_hidden, len(self.languages))

    def f_init(self, x0: nn.Constant):
        z = nn.Linear(x0, self.w0)
        h_next = nn.ReLU(z)
        return h_next

    def f_recurrent(self, h: nn.Node, x: nn.Constant) -> nn.Node:
        z = nn.Add(nn.Linear(x, self.wx), nn.Linear(h, self.wh))
        h_next = nn.ReLU(z)
        return h_next

    def f_final(self, h: nn.Node) -> nn.Node:
        logits = nn.Linear(h, self.wf)
        return logits

    def run(self, xs: [nn.Node]) -> nn.Node:
        """
        Runs the model for a batch of examples.

        Although words have different lengths, our data processing guarantees
        that within a single batch, all words will be of the same length (L).

        Here `xs` will be a list of length L. Each element of `xs` will be a
        node with shape (batch_size x self.num_chars), where every row in the
        array is a one-hot vector encoding of a character. For example, if we
        have a batch of 8 three-letter words where the last word is "cat", then
        xs[1] will be a node that contains a 1 at position (7, 0). Here the
        index 7 reflects the fact that "cat" is the last word in the batch, and
        the index 0 reflects the fact that the letter "a" is the inital (0th)
        letter of our combined alphabet for this task.

        Your model should use a Recurrent Neural Network to summarize the list
        `xs` into a single node of shape (batch_size x hidden_size), for your
        choice of hidden_size. It should then calculate a node of shape
        (batch_size x 5) containing scores, where higher scores correspond to
        greater probability of the word originating from a particular language.

        Inputs:
            xs: a list with L elements (one per character), where each element
                is a node with shape (batch_size x self.num_chars)
        Returns:
            A node with shape (batch_size x 5) containing predicted scores
                (also called logits)
        """
        h = self.f_init(xs[0])
        for x in xs[1:]:
            h = self.f_recurrent(h, x)
        logits = self.f_final(h)
        return logits

    def get_loss(self, xs: [nn.Node], y: nn.Constant) -> nn.SoftmaxLoss:
        """
        Computes the loss for a batch of examples.

        The correct labels `y` are represented as a node with shape
        (batch_size x 5). Each row is a one-hot vector encoding the correct
        language.

        Inputs:
            xs: a list with L elements (one per character), where each element
                is a node with shape (batch_size x self.num_chars)
            y: a node with shape (batch_size x 5)
        Returns: a loss node
        """
        logits = self.run(xs)
        loss = nn.SoftmaxLoss(logits, y)
        return loss

    def train(self, dataset: backend.Dataset):
        """
        Trains the model.
        """
        for epoch in range(self.epochs):
            for x, y in dataset.iterate_once(self.bs):
                loss = self.get_loss(x, y)
                grads = nn.gradients(loss, [self.w0, self.wx, self.wh, self.wf])
                self.w0.update(grads[0], -self.lr)
                self.wx.update(grads[1], -self.lr)
                self.wh.update(grads[2], -self.lr)
                self.wf.update(grads[3], -self.lr)
            if dataset.get_validation_accuracy() > 0.85:
                break
