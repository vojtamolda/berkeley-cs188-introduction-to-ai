# myAgents.py
# ---------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).

from pacman import GameState
from searchProblems import PositionSearchProblem
from game import Agent, Actions, Grid, Directions

import search


def createAgents(num_pacmen, agent='CooperativeClosestDotAgentWithMemory'):
    common = {}
    agents = [eval(agent)(index, common) for index in range(num_pacmen)]
    return agents


class ClosestDotAgent(Agent):
    """
    Basic agent that searches for the food pellet closest to the Pacman location.

    Score: 550 (Staff agents: 870, 1120)
    """
    def getAction(self, state):
        actions = self.findPathToClosestDot(state)
        return actions[0]

    def findPathToClosestDot(self, gameState: GameState) -> list:
        problem = AnyFoodSearchProblem(gameState, self.index)
        actions = search.breadthFirstSearch(problem)
        return actions


class ClosestDotAgentWithMemory(ClosestDotAgent):
    """
    Basic agent that searches for the food pellet closest to the Pacman location and
    remembers the path so it doesn't have to be recomputed every time-step.

    Score: 1020 (Staff agents: 870, 1120)
    """
    def initialize(self):
        self.actions = []
        self.closestFood = None

    def getAction(self, gameState: GameState) -> int:
        if not (self.closestFood and gameState.hasFood(*self.closestFood)):
            self.actions = self.findPathToClosestDot(gameState)
            self.actions.reverse()

            vectors = [Actions.directionToVector(action, 1) for action in self.actions]
            delta = [sum(xy) for xy in zip(*vectors)]
            pacmanPosition = gameState.getPacmanPosition(self.index)
            self.closestFood = pacmanPosition[0] + delta[0], pacmanPosition[1] + delta[1]

        return self.actions.pop()


class CooperativeClosestDotAgentWithMemory(ClosestDotAgentWithMemory):
    """
    Basic agent that searches for the food pellet closest to the Pacman location. Agent
    has a memory that keeps actions that will take it to the food location so it doesn't
    have to be recomputed ever time. Also agents share their goal food pellets so any
    particular pellet is always targeted only by one Pacman agent.

    Score: 1090 (Staff agents: 870, 1120)
    """
    def __init__(self, index: int, common: list):
        super().__init__(index)
        self.common = common

    def getAction(self, gameState: GameState) -> int:
        action = super().getAction(gameState)
        self.common[self.index] = self.closestFood
        return action

    def removeOthersFood(self, gameState: GameState) -> GameState:
        if gameState.getNumFood() <= gameState.getNumAgents():
            return gameState

        gameStateWithoutOthersFood = gameState.deepCopy()
        for index, otherFood in self.common.items():
            if index == self.index or otherFood is None:
                continue
            foodX, foodY = otherFood
            gameStateWithoutOthersFood.data.food[foodX][foodY] = False
        return gameStateWithoutOthersFood

    def findPathToClosestDot(self, gameState: GameState) -> list:
        gameStateWithoutOtherFood = self.removeOthersFood(gameState)
        actions = super().findPathToClosestDot(gameStateWithoutOtherFood)
        return actions


class FoodDistanceFieldAgent(Agent):
    """
    Agent that pre-calculates distance scalar fields that holds number of steps from
    each valid Pacman location to a particular food pellet.

    Score: 220 (Staff agents: 870, 1120)
    """
    Directions = [Directions.NORTH, Directions.SOUTH, Directions.EAST, Directions.WEST]

    def __init__(self, index: int, common: dict):
        super().__init__(index)
        self.common = common

    def initialize(self):
        self.closestFood = None
        self.closestFoodDistanceField = None

    def calculateDistanceFields(self, gameState: GameState):
        walls = gameState.getWalls()
        for (fx, fy) in gameState.getFood().asList():
            distances = [[None for y in range(gameState.getHeight())] for x in range(gameState.getWidth())]
            distances[fx][fy] = 0
            self.calculateDistances((fx, fy), walls, distances, 0)
            self.common[fx, fy] = distances

    def calculateDistances(self, position: tuple, walls: Grid, distances: list, distance: int):
        x, y = position
        for direction in self.Directions:
            dx, dy = Actions.directionToVector(direction, 1)
            nx, ny = x + dx, y + dy
            if walls[nx][ny] is True:
                continue
            if distances[nx][ny] is not None:
                continue
            distances[nx][ny] = distance + 1
            self.calculateDistances((nx, ny), walls, distances, distance + 1)

    def getAction(self, gameState: GameState) -> int:
        if not self.common:
            self.calculateDistanceFields(gameState)

        (px, py) = gameState.getPacmanPosition(self.index)
        if not (self.closestFood and gameState.hasFood(*self.closestFood)):
            distances = [(distanceField[px][py], (fx, fy)) for (fx, fy), distanceField in self.common.items()]
            _, (fx, fy) = min(distances)
            self.closestFoodDistanceField = self.common[fx, fy]
            self.closestFood = (fx, fy)
            del self.common[self.closestFood]

        distancesActions = []
        for direction in self.Directions:
            dx, dy = Actions.directionToVector(direction, 1)
            nx, ny = px + dx, py + dy
            if self.closestFoodDistanceField[nx][ny] is not None:
                distancesActions += [(self.closestFoodDistanceField[nx][ny], direction)]
        _, action = min(distancesActions)
        return action


class AnyFoodSearchProblem(PositionSearchProblem):
    """
    A search problem for finding the shortest path to any food.
    """
    def __init__(self, gameState: GameState, agentIndex: int):
        super().__init__(gameState, agentIndex, warn=False)
        self.foodList = gameState.getFood().asList()

    def isGoalState(self, state: tuple) -> bool:
        isFood = state in self.foodList
        return isFood
