# myAgent.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).
# This file was based on the starter code for student bots, and refined 
# by Mesut (Xiaocheng) Yang


import random

from util import Counter
from capture import GameState
from captureAgents import CaptureAgent


class MyAgent(CaptureAgent):
    Weights = Counter({
        'ghostInvDistance': -10,
        'deathCount': -1000,
        'foodCount': -10,
        'foodMinInvDistance': +1 / 2,
    })

    def registerInitialState(self, gameState: GameState):
        CaptureAgent.registerInitialState(self, gameState)

    def chooseAction(self, gameState: GameState) -> str:
        evaluations = {}
        for action in gameState.getLegalActions(self.index):
            successorState = gameState.generateSuccessor(self.index, action)
            features = self.extractFeatures(successorState)
            evaluation = self.Weights * features
            evaluations[action] = evaluation

        _, maxEvaluation = max(evaluations.items(), key=lambda tpl: tpl[1])
        bestActions = [action for action, evaluation in evaluations.items() if maxEvaluation == evaluation]
        return random.choice(bestActions)

    def extractFeatures(self, gameState: GameState) -> Counter:
        features = Counter()

        myself = gameState.getAgentState(self.index)
        foods = self.getFood(gameState).asList()

        for oldState in self.observationHistory[1:]:
            if oldState.getAgentPosition(self.index) == oldState.getInitialAgentPosition(self.index):
                features['deathCount'] += 1

        if myself.getPosition() == gameState.getInitialAgentPosition(self.index):
            features['deathCount'] += 1

        for index in self.getOpponents(gameState):
            opponent = gameState.getAgentState(index)
            ghostDistance = self.getMazeDistance(myself.getPosition(), opponent.getPosition())
            if ghostDistance <= 1:
                features['ghostInvDistance'] = 1 / (ghostDistance + 1)

        features['foodCount'] = len(foods) - 2
        if foods:
            foodDistances = (self.getMazeDistance(myself.getPosition(), food) for food in foods)
            features['foodMinInvDistance'] = 1 / (min(foodDistances) + 1)

        return features
