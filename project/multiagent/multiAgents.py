# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).

import math
import util
import random
import statistics
import collections
from pacman import GameState
from game import Agent, Actions, Directions


class ReflexAgent(Agent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
    """

    def getAction(self, gameState: GameState) -> str:
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {NORTH, SOUTH, WEST, EAST, STOP}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, gameState: GameState, action: str) -> float:
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        state = gameState.generatePacmanSuccessor(action)
        foods = gameState.getFood().asList()
        capsules = gameState.getCapsules()
        pacman = state.getPacmanState()
        evaluation = 0.0

        for ghost in state.getGhostStates():
            ghostDistance = util.manhattanDistance(pacman.getPosition(), ghost.getPosition())
            if ghost.scaredTimer > ghostDistance:
                evaluation += 100 / (ghostDistance + 1)
            elif ghostDistance < 2:
                evaluation -= 100 / (ghostDistance + 1)

        if foods:
            foodDistances = (util.manhattanDistance(pacman.getPosition(), food) for food in foods)
            invFoodDistances = (1 / (foodDistance + 1) for foodDistance in foodDistances)
            evaluation += statistics.mean(invFoodDistances)

        if capsules:
            capsuleDistances = (util.manhattanDistance(pacman.getPosition(), capsule) for capsule in capsules)
            invCapsuleDistances = (1 / (capsuleDistance + 1) for capsuleDistance in capsuleDistances)
            evaluation += 1 * statistics.mean(invCapsuleDistances)

        return evaluation


def scoreEvaluationFunction(gameState: GameState) -> float:
    """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
    """
    return gameState.getScore()


class MultiAgentSearchAgent(Agent):
    """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
    """

    def __init__(self, evalFn: str = 'scoreEvaluationFunction', depth: str = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)


class MinimaxAgent(MultiAgentSearchAgent):
    """
    Your minimax agent (question 2)
    """

    ActionScore = collections.namedtuple('ActionScore', ['action', 'score'])

    def getAction(self, gameState: GameState) -> str:
        """
        Returns the minimax action from the current gameState using self.depth
        and self.evaluationFunction.

        Here are some method calls that might be useful when implementing minimax.

        gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

        gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

        gameState.getNumAgents():
        Returns the total number of agents in the game

        gameState.isWin():
        Returns whether or not the game state is a winning state

        gameState.isLose():
        Returns whether or not the game state is a losing state
        """
        optimal = self.minimax(gameState, self.index, 1)
        return optimal.action

    @staticmethod
    def maximum(candidate: ActionScore, maximum: ActionScore) -> ActionScore:
        if maximum.score is None:
            return candidate
        if candidate.score > maximum.score:
            maximum = candidate
        return maximum

    @staticmethod
    def minimum(candidate: ActionScore, minimum: ActionScore) -> ActionScore:
        if minimum.score is None:
            return candidate
        if candidate.score < minimum.score:
            minimum = candidate
        return minimum

    def minimax(self, gameState: GameState, agentIndex: int, depth: int) -> ActionScore:
        if agentIndex >= gameState.getNumAgents():
            agentIndex = 0
            depth += 1

        if depth > self.depth or gameState.isWin() or gameState.isLose():
            evaluation = self.evaluationFunction(gameState)
            return self.ActionScore(None, evaluation)

        minMax = self.maximum if agentIndex == 0 else self.minimum

        optimal = self.ActionScore(None, None)
        for action in gameState.getLegalActions(agentIndex):
            succesorState = gameState.generateSuccessor(agentIndex, action)
            current = self.minimax(succesorState, agentIndex + 1, depth)
            candidate = self.ActionScore(action, current.score)
            optimal = minMax(candidate, optimal)

        return optimal


class AlphaBetaAgent(MinimaxAgent):
    """
    Your minimax agent with alpha-beta pruning (question 3)
    """

    def alphaPrune(self, candidate: MinimaxAgent.ActionScore, minimum: MinimaxAgent.ActionScore,
                   alpha: float, beta: float) -> tuple:
        minimum = self.minimum(candidate, minimum)
        beta = min(beta, minimum.score)
        prune = (minimum.score < alpha)
        return minimum, alpha, beta, prune

    def betaPrune(self, candidate: MinimaxAgent.ActionScore, maximum: MinimaxAgent.ActionScore,
                  alpha: float, beta: float) -> tuple:
        maximum = self.maximum(candidate, maximum)
        alpha = max(alpha, maximum.score)
        prune = (maximum.score > beta)
        return maximum, alpha, beta, prune

    def minimax(self, gameState: GameState, agentIndex: int, depth: int,
                alpha: float = -math.inf, beta: float = +math.inf) -> MinimaxAgent.ActionScore:
        if agentIndex >= gameState.getNumAgents():
            agentIndex = 0
            depth += 1

        if depth > self.depth or gameState.isWin() or gameState.isLose():
            evaluation = self.evaluationFunction(gameState)
            return self.ActionScore(None, evaluation)

        aplhaBetaPrune = self.betaPrune if agentIndex == 0 else self.alphaPrune

        optimal = self.ActionScore(None, None)
        for action in gameState.getLegalActions(agentIndex):
            succesorState = gameState.generateSuccessor(agentIndex, action)
            current = self.minimax(succesorState, agentIndex + 1, depth, alpha, beta)
            candidate = self.ActionScore(action, current.score)

            optimal, alpha, beta, prune = aplhaBetaPrune(candidate, optimal, alpha, beta)
            if prune is True:
                return optimal

        return optimal


class ExpectimaxAgent(MinimaxAgent):
    """
    Your expectimax agent (question 4)
    """

    @staticmethod
    def minimum(candidate: MinimaxAgent.ActionScore, mean: MinimaxAgent.ActionScore) -> MinimaxAgent.ActionScore:
        if mean.score is None:
            mean = candidate
        mean = MinimaxAgent.ActionScore(None, (candidate.score + mean.score) / 2)
        return mean


def betterEvaluationFunction(gameState: GameState) -> float:
    """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: <write something here so we know what you did>
    """
    evaluation = 0.0

    if gameState.isLose():
        return -math.inf
    if gameState.isWin():
        return gameState.getScore()

    foods = gameState.getFood().asList()
    evaluation -= 1 * len(foods)
    if foods:
        foodDistances = (util.manhattanDistance(gameState.getPacmanPosition(), food) for food in foods)
        invFoodDistances = (1 / (foodDistance + 1) for foodDistance in foodDistances)
        evaluation += statistics.mean(invFoodDistances)

    evaluation -= 500 * len(gameState.getCapsules()) * (gameState.getNumAgents() - 1)

    return evaluation


# Abbreviation
better = betterEvaluationFunction
