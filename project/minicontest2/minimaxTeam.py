import math
import statistics
import collections

from util import Counter
from capture import GameState
from reflexTeam import ReflexAgent


def createTeam(firstIndex: int, secondIndex: int, isRed: bool,
               first: str = None, second: str = None):
    return [PacmanMinimaxPruningAgent(firstIndex), GhostMinimaxPruningAgent(secondIndex)]


# region Minimax Agents


class MinimaxAgent(ReflexAgent):
    ActionScore = collections.namedtuple('ActionScore', ['action', 'score'])
    Depth = 2

    def chooseAction(self, gameState: GameState) -> str:
        optimal = self.minimax(gameState, self.index, 1)
        return optimal.action

    @staticmethod
    def maximum(candidate: ActionScore, maximum: ActionScore) -> ActionScore:
        if maximum.score is None:
            return candidate
        if candidate.score > maximum.score:
            maximum = candidate
        return maximum

    @staticmethod
    def minimum(candidate: ActionScore, minimum: ActionScore) -> ActionScore:
        if minimum.score is None:
            return candidate
        if candidate.score < minimum.score:
            minimum = candidate
        return minimum

    def minimax(self, gameState: GameState, agentIndex: int, depth: int) -> ActionScore:
        if agentIndex >= gameState.getNumAgents():
            agentIndex = 0
            depth += 1

        if depth > self.Depth:
            features = self.extractFeatures(gameState)
            evaluation = self.Weights * features
            return self.ActionScore(None, evaluation)

        minMax = self.maximum if agentIndex == 0 else self.minimum

        optimal = self.ActionScore(None, None)
        self.observationHistory += [gameState]

        for action in gameState.getLegalActions(agentIndex):
            succesorState = gameState.generateSuccessor(agentIndex, action)
            current = self.minimax(succesorState, agentIndex + 1, depth)
            candidate = self.ActionScore(action, current.score)
            optimal = minMax(candidate, optimal)

        self.observationHistory.pop()
        return optimal


class PacmanMinimaxAgent(MinimaxAgent):
    Weights = Counter({
        'normalGhostInvDistance': -10,
        'scaredGhostInvDistance': +100,
        'deathCount': -1000,
        'killCount': +1000,

        'score': +5000,
        'foodCount': -10,
        'capsuleCount': -10,
        'homeInvDistance': +5000,

        'foodMinInvDistance': +1/2,
        'capsuleMinInvDistance': +1,
    })


class GhostMinimaxAgent(MinimaxAgent):
    Weights = Counter({
        'normalPacmanInvDistance': +100,
        'scaryPacmanInvDistance': -100,
        'normalGhostInvDistance': -10,
        'scaredGhostInvDistance': +10,
        'deathCount': -1000,
        'killCount': +1000,

        'score': +10,
        'foodCount': +10,
        'capsuleCount': +10,

        'capsuleMinInvDistance': +1,
    })

    def getFood(self, gameState: GameState) -> list:
        return self.getFoodYouAreDefending(gameState)

    def getCapsules(self, gameState: GameState) -> list:
        return self.getCapsulesYouAreDefending(gameState)


# endregion


# region Minimax Pruning Agents


class MinimaxPruningAgent(MinimaxAgent):
    ActionScore = MinimaxAgent.ActionScore

    def alphaPrune(self, candidate: ActionScore, minimum: ActionScore,
                   alpha: float, beta: float) -> tuple:
        minimum = self.minimum(candidate, minimum)
        beta = min(beta, minimum.score)
        prune = (minimum.score < alpha)
        return minimum, alpha, beta, prune

    def betaPrune(self, candidate: ActionScore, maximum: ActionScore,
                  alpha: float, beta: float) -> tuple:
        maximum = self.maximum(candidate, maximum)
        alpha = max(alpha, maximum.score)
        prune = (maximum.score > beta)
        return maximum, alpha, beta, prune

    def minimax(self, gameState: GameState, agentIndex: int, depth: int,
                alpha: float = -math.inf, beta: float = +math.inf) -> ActionScore:
        if agentIndex >= gameState.getNumAgents():
            agentIndex = 0
            depth += 1

        if depth > self.Depth:
            features = self.extractFeatures(gameState)
            evaluation = self.Weights * features
            return self.ActionScore(None, evaluation)

        aplhaBetaPrune = self.betaPrune if agentIndex in self.getTeam(gameState) else self.alphaPrune

        optimal = self.ActionScore(None, None)
        self.observationHistory += [gameState]

        for action in gameState.getLegalActions(agentIndex):
            succesorState = gameState.generateSuccessor(agentIndex, action)
            current = self.minimax(succesorState, agentIndex + 1, depth, alpha, beta)
            candidate = self.ActionScore(action, current.score)

            optimal, alpha, beta, prune = aplhaBetaPrune(candidate, optimal, alpha, beta)
            if prune is True:
                break

        self.observationHistory.pop()
        return optimal


class PacmanMinimaxPruningAgent(MinimaxPruningAgent):
    Weights = PacmanMinimaxAgent.Weights


class GhostMinimaxPruningAgent(MinimaxPruningAgent):
    Weights = GhostMinimaxAgent.Weights


# endregion
