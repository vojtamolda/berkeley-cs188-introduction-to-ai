import random
import statistics

from util import Counter
from capture import GameState
from captureAgents import CaptureAgent


def createTeam(firstIndex: int, secondIndex: int, isRed: bool,
               first: str = None, second: str = None):
    return [PacmanReflexAgent(firstIndex), GhostReflexAgent(secondIndex)]


class ReflexAgent(CaptureAgent):
    Weights = Counter()

    def registerInitialState(self, gameState: GameState):
        CaptureAgent.registerInitialState(self, gameState)

    def chooseAction(self, gameState: GameState) -> str:
        evaluations = {}
        for action in gameState.getLegalActions(self.index):
            successorState = gameState.generateSuccessor(self.index, action)
            features = self.extractFeatures(successorState)
            evaluation = self.Weights * features
            evaluations[action] = evaluation

        _, maxEvaluation = max(evaluations.items(), key=lambda tpl: tpl[1])
        bestActions = [action for action, evaluation in evaluations.items() if maxEvaluation == evaluation]
        return random.choice(bestActions)

    def extractFeatures(self, gameState: GameState) -> Counter:
        features = Counter()

        myself = gameState.getAgentState(self.index)
        foods = self.getFood(gameState).asList()
        capsules = self.getCapsules(gameState)

        features['score'] = self.getScore(gameState)
        for oldState in self.observationHistory[1:]:
            if oldState.getAgentPosition(self.index) == oldState.getInitialAgentPosition(self.index):
                features['deathCount'] += 1
            for index in self.getOpponents(oldState):
                if oldState.getAgentPosition(index) == oldState.getInitialAgentPosition(index):
                    features['killCount'] += 1

        if myself.getPosition() == gameState.getInitialAgentPosition(self.index):
            features['deathCount'] += 1

        for index in self.getOpponents(gameState):
            opponent = gameState.getAgentState(index)
            if opponent.getPosition() == gameState.getInitialAgentPosition(index):
                features['killCount'] += 1

            if opponent.isPacman:
                pacmanDistance = self.getMazeDistance(myself.getPosition(), opponent.getPosition())
                if myself.scaredTimer > pacmanDistance:
                    features['scaryPacmanInvDistance'] = 1 / (pacmanDistance + 1)
                else:
                    features['normalPacmanInvDistance'] = 1 / (pacmanDistance + 1)
            else:
                ghostDistance = self.getMazeDistance(myself.getPosition(), opponent.getPosition())
                if opponent.scaredTimer > ghostDistance:
                    features['scaredGhostInvDistance'] = 1 / (ghostDistance + 1)
                elif ghostDistance <= 1:
                    features['normalGhostInvDistance'] = 1 / (ghostDistance + 1)

        if myself.numCarrying > 1:
            home = gameState.getInitialAgentPosition(self.index)
            homeDistance = self.getMazeDistance(myself.getPosition(), home)
            features['homeInvDistance'] = 1 / (homeDistance + 1)

        features['foodCount'] = len(foods) - 2
        if foods:
            foodDistances = (self.getMazeDistance(myself.getPosition(), food) for food in foods)
            features['foodMinInvDistance'] = 1 / (min(foodDistances) + 1)

        features['capsuleCount'] = len(capsules)
        if capsules:
            capsuleDistances = (self.getMazeDistance(myself.getPosition(), capsule) for capsule in capsules)
            features['capsuleMinInvDistance'] = 1 / (min(capsuleDistances) + 1)

        return features


class PacmanReflexAgent(ReflexAgent):
    Weights = Counter({
        'normalGhostInvDistance': -10,
        'scaredGhostInvDistance': +100,
        'deathCount': -1000,
        'killCount': +1000,

        'score': +5000,
        'foodCount': -10,
        'capsuleCount': -10,
        'homeInvDistance': +5000,

        'foodMinInvDistance': +1/2,
        'capsuleMinInvDistance': +1,
    })


class GhostReflexAgent(ReflexAgent):
    Weights = Counter({
        'normalPacmanInvDistance': +100,
        'scaryPacmanInvDistance': -100,
        'normalGhostInvDistance': -10,
        'scaredGhostInvDistance': +10,
        'deathCount': -1000,
        'killCount': +1000,

        'score': +10,
        'foodCount': +10,
        'capsuleCount': +10,

        'capsuleMinInvDistance': +1,
    })

    def getFood(self, gameState: GameState) -> list:
        return self.getFoodYouAreDefending(gameState)

    def getCapsules(self, gameState: GameState) -> list:
        return self.getCapsulesYouAreDefending(gameState)
