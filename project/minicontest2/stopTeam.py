from game import Directions
from capture import GameState
from captureAgents import CaptureAgent


def createTeam(firstIndex: int, secondIndex: int, isRed: bool,
               first: str = None, second: str = None):
    return [StopAgent(firstIndex), StopAgent(secondIndex)]


class StopAgent(CaptureAgent):
    def registerInitialState(self, gameState: GameState):
        CaptureAgent.registerInitialState(self, gameState)

    def chooseAction(self, gameState: GameState) -> str:
        return Directions.STOP
