import random

from capture import GameState
from captureAgents import CaptureAgent


def createTeam(firstIndex: int, secondIndex: int, isRed: bool,
               first: str = None, second: str = None):
    return [RandomAgent(firstIndex), RandomAgent(secondIndex)]


class RandomAgent(CaptureAgent):
    def registerInitialState(self, gameState: GameState):
        CaptureAgent.registerInitialState(self, gameState)

    def chooseAction(self, gameState: GameState) -> str:
        legalActions = gameState.getLegalActions(self.index)
        return random.choice(legalActions)
