# qlearningAgents.py
# ------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).

import util
import random
from pacman import GameState
from featureExtractors import *
from learningAgents import ReinforcementAgent


class QLearningAgent(ReinforcementAgent):
    """
      Q-Learning Agent

      Functions you should fill in:
        - computeValueFromQValues
        - computeActionFromQValues
        - getQValue
        - getAction
        - update

      Instance variables you have access to
        - self.epsilon (exploration prob)
        - self.alpha (learning rate)
        - self.discount (discount rate)

      Functions you should use
        - self.getLegalActions(state)
          which returns legal actions for a state
    """
    def __init__(self, **args):
        ReinforcementAgent.__init__(self, **args)
        self.qValues = {}

    def getQValue(self, state: tuple, action: str) -> float:
        """
          Returns Q(state,action)
          Should return 0.0 if we have never seen a state
          or the Q node value otherwise
        """
        if state not in self.qValues:
            self.qValues[state] = util.Counter()
        return self.qValues[state][action]

    def computeValueFromQValues(self, state: tuple) -> float:
        """
          Returns max_action Q(state,action)
          where the max is over legal actions.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return a value of 0.0.
        """
        legalActions = self.getLegalActions(state)
        if not legalActions:
            return 0.0
        else:
            qValues = [self.getQValue(state, action) for action in legalActions]
            maxQValue = max(qValues)
            return maxQValue

    def computeActionFromQValues(self, state: tuple) -> str:
        """
          Compute the best action to take in a state.  Note that if there
          are no legal actions, which is the case at the terminal state,
          you should return None.
        """
        legalActions = self.getLegalActions(state)
        if not legalActions:
            return None
        else:
            qValuesActions = [(self.getQValue(state, action), action) for action in legalActions]
            maxQValue, _ = max(qValuesActions)
            maxActions = [action for qValue, action in qValuesActions if qValue == maxQValue]
            maxAction = random.choice(maxActions)
            return maxAction

    def getAction(self, state: tuple) -> str:
        """
          Compute the action to take in the current state.  With
          probability self.epsilon, we should take a random action and
          take the best policy action otherwise.  Note that if there are
          no legal actions, which is the case at the terminal state, you
          should choose None as the action.

          HINT: You might want to use util.flipCoin(prob)
          HINT: To pick randomly from a list, use random.choice(list)
        """
        # Pick Action
        if util.flipCoin(self.epsilon):
            action = random.choice(self.getLegalActions(state))
        else:
            action = self.computeActionFromQValues(state)
        return action

    def update(self, state: tuple, action: str, nextState: tuple, reward: float):
        """
          The parent class calls this to observe a
          state = action => nextState and reward transition.
          You should do your Q-Value update here

          NOTE: You should never call this function,
          it will be called on your behalf
        """
        currentQValue = self.getQValue(state, action)
        updatedQValue = reward + self.discount * self.computeValueFromQValues(nextState)
        self.qValues[state][action] = (1 - self.alpha) * currentQValue + self.alpha * updatedQValue

    def getPolicy(self, state: tuple) -> str:
        return self.computeActionFromQValues(state)

    def getValue(self, state: tuple) -> float:
        return self.computeValueFromQValues(state)


class PacmanQAgent(QLearningAgent):
    "Exactly the same as QLearningAgent, but with different default parameters"

    def __init__(self, epsilon: float = 0.05, gamma: float = 0.8, alpha: float = 0.2, numTraining: int = 0, **args):
        """
        These default parameters can be changed from the pacman.py command line.
        For example, to change the exploration rate, try:
            python pacman.py -p PacmanQLearningAgent -a epsilon=0.1

        alpha    - learning rate
        epsilon  - exploration rate
        gamma    - discount factor
        numTraining - number of training episodes, i.e. no learning after these many episodes
        """
        args['epsilon'] = epsilon
        args['gamma'] = gamma
        args['alpha'] = alpha
        args['numTraining'] = numTraining
        self.index = 0  # This is always Pacman
        QLearningAgent.__init__(self, **args)

    def getAction(self, state: GameState) -> str:
        """
        Simply calls the getAction method of QLearningAgent and then
        informs parent of action for Pacman.  Do not change or remove this
        method.
        """
        action = QLearningAgent.getAction(self, state)
        self.doAction(state, action)
        return action


class ApproximateQAgent(PacmanQAgent):
    """
       ApproximateQLearningAgent

       You should only have to overwrite getQValue
       and update.  All other QLearningAgent functions
       should work as is.
    """
    def __init__(self, extractor: str = 'IdentityExtractor', **args):
        self.featExtractor = util.lookup(extractor, globals())()
        PacmanQAgent.__init__(self, **args)
        self.weights = util.Counter()

    def getWeights(self):
        return self.weights

    def getQValue(self, state: GameState, action: str) -> float:
        """
          Should return Q(state,action) = w * featureVector
          where * is the dotProduct operator
        """
        features = self.featExtractor.getFeatures(state, action)
        qValue = self.weights * features
        return qValue

    def update(self, state: GameState, action: str, nextState: GameState, reward: float):
        """
           Should update your weights based on transition
        """
        currentQValue = self.getQValue(state, action)
        updatedQValue = reward + self.discount * self.computeValueFromQValues(nextState)
        difference = updatedQValue - currentQValue

        if difference != 0 and self.alpha != 0:
            weightsUpdate = self.featExtractor.getFeatures(state, action)
            weightsUpdate.divideAll(1 / self.alpha / difference)
            self.weights += weightsUpdate

    def final(self, state: tuple):
        "Called at the end of each game."
        # call the super-class final method
        PacmanQAgent.final(self, state)

        # did we finish training?
        if self.episodesSoFar == self.numTraining:
            # you might want to print your weights here for debugging
            print(self.weights)
