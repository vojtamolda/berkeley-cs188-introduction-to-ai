# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import mdp
import util
import collections

from learningAgents import ValueEstimationAgent


class ValueIterationAgent(ValueEstimationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A ValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp: mdp.MarkovDecisionProcess, discount: float = 0.9, iterations: int = 100):
        """
          Your value iteration agent should take an mdp on
          construction, run the indicated number of iterations
          and then act according to the resulting policy.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state, action, nextState)
              mdp.isTerminal(state)
        """
        self.mdp = mdp
        self.discount = discount
        self.iterations = iterations
        self.values = util.Counter()  # A Counter is a dict with default 0
        self.runValueIteration()

    def runValueIteration(self):
        for iter in range(self.iterations):
            values = self.values.copy()

            for state in self.mdp.getStates():
                if self.mdp.isTerminal(state):
                    values[state] = self.mdp.getReward(state, None, None)
                    continue
                actionQValues = []
                for action in self.mdp.getPossibleActions(state):
                    actionQValue = self.computeQValueFromValues(state, action)
                    actionQValues += [actionQValue]
                values[state] = max(actionQValues)

            self.values = values

    def getValue(self, state: tuple) -> float:
        """
          Return the value of the state (computed in __init__).
        """
        return self.values[state]

    def computeQValueFromValues(self, state: tuple, action: str) -> float:
        """
          Compute the Q-value of action in state from the
          value function stored in self.values.
        """
        qValue = 0
        for transition, prob in self.mdp.getTransitionStatesAndProbs(state, action):
            reward = self.mdp.getReward(state, action, transition)
            qValue += prob * (reward + self.discount * self.values[transition])
        return qValue

    def computeActionFromValues(self, state: tuple) -> str:
        """
          The policy is the best action in the given state
          according to the values currently stored in self.values.

          You may break ties any way you see fit.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return None.
        """
        possibleActions = self.mdp.getPossibleActions(state)
        if not possibleActions:
            return None

        qValues = ((self.computeQValueFromValues(state, action), action) for action in possibleActions)
        (qValue, action) = max(qValues)
        return action

    def getPolicy(self, state: tuple) -> str:
        return self.computeActionFromValues(state)

    def getAction(self, state:tuple ) -> str:
        """
          Returns the policy at the state (no exploration).
        """
        return self.computeActionFromValues(state)

    def getQValue(self, state: tuple, action: str) -> float:
        return self.computeQValueFromValues(state, action)


class AsynchronousValueIterationAgent(ValueIterationAgent):
    """
        * Please read learningAgents.py before reading this.*

        An AsynchronousValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs cyclic value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp: mdp.MarkovDecisionProcess, discount: float = 0.9, iterations: int = 1000):
        """
          Your cyclic value iteration agent should take an mdp on
          construction, run the indicated number of iterations,
          and then act according to the resulting policy. Each iteration
          updates the value of only one state, which cycles through
          the states list. If the chosen state is terminal, nothing
          happens in that iteration.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state)
              mdp.isTerminal(state)
        """
        ValueIterationAgent.__init__(self, mdp, discount, iterations)

    def runValueIteration(self):
        for iter in range(self.iterations):
            values = self.values.copy()
            states = self.mdp.getStates()
            state = states[iter % len(states)]

            if self.mdp.isTerminal(state):
                values[state] = self.mdp.getReward(state, None, None)
                continue
            actionQValues = []
            for action in self.mdp.getPossibleActions(state):
                actionQValue = self.computeQValueFromValues(state, action)
                actionQValues += [actionQValue]
            values[state] = max(actionQValues)

            self.values = values


class PrioritizedSweepingValueIterationAgent(AsynchronousValueIterationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A PrioritizedSweepingValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs prioritized sweeping value iteration
        for a given number of iterations using the supplied parameters.
    """
    def __init__(self, mdp: mdp.MarkovDecisionProcess, discount: float = 0.9, iterations :int = 100,
                 theta : float = 1e-5):
        """
          Your prioritized sweeping value iteration agent should take an mdp on
          construction, run the indicated number of iterations,
          and then act according to the resulting policy.
        """
        self.theta = theta
        ValueIterationAgent.__init__(self, mdp, discount, iterations)

    def runValueIteration(self):
        predecesors = {state: set() for state in self.mdp.getStates()}
        for state in self.mdp.getStates():
            for action in self.mdp.getPossibleActions(state):
                for transition, _ in self.mdp.getTransitionStatesAndProbs(state, action):
                    predecesors[transition].add(state)

        queue = util.PriorityQueue()

        for state in self.mdp.getStates():
            if self.mdp.isTerminal(state):
                continue

            actionQValues = []
            for action in self.mdp.getPossibleActions(state):
                actionQValue = self.computeQValueFromValues(state, action)
                actionQValues += [actionQValue]
            diff = abs(self.values[state] - max(actionQValues))
            queue.push(state, -diff)

        for iter in range(self.iterations):
            if queue.isEmpty():
                return

            state = queue.pop()
            if self.mdp.isTerminal(state):
                continue

            actionQValues = []
            for action in self.mdp.getPossibleActions(state):
                actionQValue = self.computeQValueFromValues(state, action)
                actionQValues += [actionQValue]
            self.values[state] = max(actionQValues)

            for predecesor in predecesors[state]:
                actionQValues = []
                for action in self.mdp.getPossibleActions(predecesor):
                    actionQValue = self.computeQValueFromValues(predecesor, action)
                    actionQValues += [actionQValue]
                diff = abs(self.values[predecesor] - max(actionQValues))

                if diff > self.theta:
                    queue.update(predecesor, -diff)
